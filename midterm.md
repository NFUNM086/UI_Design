# 《视觉界面设计》期中项目
## 小组成员
徐嘉慧 黄嘉慧 何畅

## 一、 竞品分析——BOSS直聘app
### 1.1 架构层
**核心功能的简要信息架构图：**
（对核心功能和重要流程进行概括总结，方便以后有需要的时候快速调取）

![核心功能的简要信息架构图](https://images.gitee.com/uploads/images/2020/0505/162803_aed5d927_1858219.png "核心功能的简要信息架构图.png")

**细分页面的结构框架：**
（完整的信息架构图）

![细分页面的结构框架](https://images.gitee.com/uploads/images/2020/0505/162922_a0ea5e0e_1858219.png "细分页面的结构框架.png")

#### 小结
**在用户需求层面，招聘类平台的用户核心需求是更有效率地找到合适工作**。作为一个招聘类app，BOSS直聘的功能都十分齐全。

① 从总体的信息架构能看出，BOSS直聘将“职位”作为app的首页展示，且设置了“搜索”“推荐”和“热门”的岗位，以便尽最大可能为用户找到意向且合适的职位。

② 为更高效的展示职位，职位信息以垂直列表的形式展示，公司名称、工作地点、经验要求、薪资待遇等重要信息优先展示，也有细分筛选项，方便用户快速选择。

③ 再者，为了提高求职反馈效率，在职位页设置了“立即沟通”的功能，沟通界面也有设置例如“交换微信”“常用语”等更便捷、更人性化的功能与职位发布者沟通，**验证了BOSS直聘的slogan“用聊天的方式找工作”**，通过较平等和直接的沟通方式可以精准定位职位最优人选，将招聘时长缩到最短，提升了找工作和招聘的效率。

但BOSS直聘也有做得不好的地方，例如某些可视化图表中的某些指代性数据没有清楚解释具体的意思，在这方面可以完善一下；另外，聊天页面只显示30天内的沟通记录，这对于部分用户想查找一个月以前的记录并不友好。

### 1.2 交互路径分析
![交互路径分析](https://images.gitee.com/uploads/images/2020/0509/135110_6e96b7b4_1648160.jpeg "交互路径分析.jpg")

### 交互亮点提取
![交互亮点](https://images.gitee.com/uploads/images/2020/0511/151144_604b91b5_1648160.png "截图.png")

### 1.3 视觉层面分析

![视觉层面分析](https://images.gitee.com/uploads/images/2020/0505/221933_ccbd7cb3_1648228.png "BOSS直聘界面设计.png")

## 二、自己作品————聘聘APP
### 2.1 结构层

**主导航的信息架构简要列明：**

![架构图](https://images.gitee.com/uploads/images/2020/0505/222145_c5aa9857_1648228.png "5eb03a2c07912906fa50db3d.png")

**细分页面的结构框架：**

![架构详细图表](https://images.gitee.com/uploads/images/2020/0505/222224_e5135ba4_1648228.png "聘聘APP.png")

### 2.2 交互路径

![找职位找公司](https://images.gitee.com/uploads/images/2020/0512/110613_3fccdea7_1648160.jpeg "交互路径1.jpg")

![招聘沟通](https://images.gitee.com/uploads/images/2020/0512/125250_b23b88ff_1648160.jpeg "招聘交流沟通.jpg")

![搜索活动](https://images.gitee.com/uploads/images/2020/0512/132339_77e605cd_1648160.jpeg "搜索活动.jpg")

### 交互亮点简述

![交互亮点](https://images.gitee.com/uploads/images/2020/0512/150417_76ec003c_1648160.jpeg "交互亮点.jpg")

### 2.3 视觉规范

![视觉规范1](https://images.gitee.com/uploads/images/2020/0512/135016_b83f91f3_1858219.jpeg "聘聘视觉规范-02.jpg")

![视觉规范2](https://images.gitee.com/uploads/images/2020/0512/135419_80348a9b_1858219.jpeg "聘聘视觉规范-03.jpg")

![视觉规范3](https://images.gitee.com/uploads/images/2020/0512/135051_37268fb7_1858219.jpeg "聘聘视觉规范-04.jpg")

![视觉规范4](https://images.gitee.com/uploads/images/2020/0512/135116_e8040c28_1858219.jpeg "聘聘视觉规范-05.jpg")